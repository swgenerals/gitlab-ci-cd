# 3분 GitLab CI/CD

## GitLab CI/CD의 사용 사례?
초기에 로컬 린팅(local linting)과 사전 커밋 후크(pre-commit hook)는 개별 개발자가 변경을 수행하기 전에 구문과 스타일 문제를 신속하고 즉각적으로 파악할 수 있는 방법이다. 코드베이스가 점점 커지고 복잡해지며 개발팀이 더 많은 시나리오와 과제에 직면함에 따라 코드 품질 보증에 대한 보다 포괄적이고 중앙 집중적인 접근이 필요하다.

바로 여기서 CI/CD가 팔요하다. CI/CD는 코드베이스 상에서 다양한 검사, 테스트 및 태스크를 실행하기 위한 체계적이고 자동화된 프로세스를 제공한다. 코드 품질을 위한 "이중 검사" 또는 "이중 보증"의 형태로 작동한다. CI/CD 파이프라인은 기본 구문과 스타일 이슈가 포착될 뿐만 아니라 일관되고 제어되는 환경에서 보다 광범위한 테스트 세트(단위 테스트, 통합 테스트 등)가 실행되도록 보장한다.

또한 일반적인 경우에도 팀은 개발 프로세스 초기에 CI/CD를 채택하여 처음부터 견고하고 표준화된 워크플로우를 구축할 수 있다.

## 이렇게 작동한다

```
                       +-----------------+
                       |                 |
                       | GitLab Instance |
                       |                 |
                       +--------+--------+
                                |
                           (1)  | Push changes
                                |
                                v
                       +--------+--------+
                       |                 |
                       | .gitlab-ci.yml  |
                       |                 |
                       +--------+--------+
                                |
                           (2)  | Pipeline Triggered
                                |
                                v
                       +--------+--------+
                       |                 |
                       | GitLab Runner   |
                       |                 |
                       +--------+--------+
                                |
                           (3)  | Execute Jobs
                                |
                                v
                       +--------+--------+
                       |                 |
                       |    Jobs &      |
                       |   Stages Run    |
                       +--------+--------+
                                |
                           (4)  | Artifacts
                                |
                                v
                       +--------+--------+
                       |                 |
                       |   Pipeline      |
                       |     Status      |
                       +--------+--------+
```

파이프라인(워크플로우)을 설명하는 `.gitlab-ci.yml` 파일에 대해 살펴보겠다. 이는 GitLab CI/CD에서 사용하는 구성 파일이다.

## 1. YAML과 함께
JSON은 컴퓨터 간 통신(시스템 간의 데이터 교환과 구성 포맷)에 자주 사용되며, JSON이 쉽지는 않지만 모든 개발자는 JSON을 알아야 한다.

```json
{
  "name": "Ricky Gervais",
  "age": 62,
  "is_standup_comedian": true,
  "jokes": ["sarcasm", "racial", "repartee"],
  "born": {
    "city": "Whitley, Reading",
    "nation": "United Kingdom"
  },
  "reknown-work": {
    "tv_shows": [
      {
        "title": "The Office",
        "role": "Creator, Writer, Actor"
      },
      {
        "title": "Extras",
        "role": "Creator, Writer, Actor"
      }
    ],
    "movies": [
      {
        "title": "The Invention of Lying",
        "role": "Director, Writer, Actor"
      },
      {
        "title": "Ghost Town",
        "role": "Lead Actor"
      }
    ]
  }
}
```

데이터 구조를 구분하기 위해 괄호나 쉼표를 사용하는 것을 싫어하는 사람들을 위해 여기 YAML이 있다. Readme 파일에도 사용되며, 덜 장황하고 사람이 읽을 수 있는 데이터 직렬화 형식(요소 계층을 정의하기 위해 들여쓰기 수준에 의존)이다.

```yml
name: "Ricky Gervais"
age: 62
is_standup_comedian: true
jokes:
  - "sarcasm"
  - "racial"
  - "repartee"
born:
  city: "Whitley, Reading"
  nation: "United Kingdom"
reknown-work:
  tv_shows:
    - title: "The Office"
      role: "Creator, Writer, Actor"
    - title: "Extras"
      role: "Creator, Writer, Actor"
  movies:
    - title: "The Invention of Lying"
      role: "Director, Writer, Actor"
    - title: "Ghost Town"
      role: "Lead Actor"
```

그리고 여전히 키워드 사용에 대한 공식 가이드를 스크롤해야 필요도 있다. 미안해요! https://docs.gitlab.com/ee/ci/yaml/index.html

이제 GitLab Runners를 설치해야 한다(Go로 작성된 YAML 파일을 실행하는 고된 작업을 누가 수행하여야 하지만 누가 신경 쓸까요?)

macOS의 경우: https://docs.gitlab.com/runner/install/osx.html

세 번째 계단에서 막힐 수도 있어요,

```bash
$ sudo curl --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-arm64"
```

그리고 

```bash
$ sudo chmod +x /usr/local/bin/gitlab-runner
```

아무 일도 없다면, Runner를 등록해야 한다.

macOS의 경우 다음과 같이 수행한다.

```bash
$ gitlab-runner register
```

https://docs.gitlab.com/runner/register/index.html?tab=macOS을 수행한다. 그리고 https://docs.gitlab.com/ee/ci/runners/runners_scope.html을 수행하여 필요한 데드 토큰을 찾는다. 

이후 Runner가 성공적으로 등록되었으면 등록 과정에서 Runner에 필요한 설정을 포함하는 Config.toml을 생성하게 된다.

## 다음 단계

- (`sudo gitlab-runner start`를 사용하여) Gitlab Runner를 서비스로 설정한 경우 시스템이 시작될 때 자동으로 백그라운드에서 실행되며 `gitlab-runner run`을 실행할 필요가 없다.
- 문제 해결 중이거나 러너 로그를 대화식으로 보고 싶다면 `gitlab-runner run`을 별도로 사용할 수 있다.

그런 다음 편집기를 사용하여 다음 구성 상태를 확인한다.

```bash
$ vim /Users/yourusername/.gitlab-runner/config.toml
```

파이프라인 단계에 대해서는 [다음 포스팅](./part-2.md)에서 자세히 설명하겠다.
