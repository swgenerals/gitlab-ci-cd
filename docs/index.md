# GitLab CI/CD<sup>[1](#footnote_1)</sup>

CI(Continuous Integration)와 CD(Continuous Deployment)의 본질은 협업 중에 발생하는 실수를 방지하고 체계적이고 자동적인 코드 검증 프로세스를 만드는 것이다.

1. [3분 GitLab CI/CD](./part-1.md)
1. [Gitlab CI/CD 단계](./part-2.md)

<a name="footnote_1">1</a>: [GitLab CI/CD in 3 mins (1)](https://medium.com/@colliethecocky/gitlab-ci-cd-in-3-mins-1-57e92533ff45)와 [Gitlab CI/CD stages(2)](https://medium.com/@colliethecocky/gitlab-ci-cd-stages-2-c08d887e13df)를 편역하였다.
