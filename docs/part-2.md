# Gitlab CI/CD 단계
이제 다음 내용을 자세히 살펴보도록 하겠다.

```yml
stages:
  - build
  - test
  - deploy
  # Add more stages as needed

variables:
  # Define variables applicable to the entire pipeline

before_script:
  # Commands to be run before each job
  # common setup, environment preparation, authentication, installation
  # Any other configuration steps that are common to multiple jobs can be placed in this stage

build:
  # Build-related commands

test:
  # Test-related commands

after_script:
  # Commands to be run after each job

deploy:
  # Deployment-related commands
```

## `variables`
이것들은 전체 파이프라인 동안 액세스할 수 있다. API 키, 비밀번호 또는 토큰과 같은 민감한 정보를 저장하는 데 자주 사용된다. 다음은 **작업 수준 변수(job-level variables)**(단계 내부 설정)이다.

```yml
build-job:
  variables:
    BUILD_ENV: "development"
  script:
    - echo "Building in $BUILD_ENV environment..."
```

도커(Docker)를 예로 들어보자. 다음은 GitLab CI/CD 파이프라인에서 도커가 필요할 때 일반적인 단계를 요약한 것이다. 파이프라인 레벨 변수는 `.gitlab-ci.yml` 파일의 최상위 레벨에서 특정 단계나 작업 외부에서 정의된다. 이 변수들은 파이프라인 내의 모든 단계와 작업에 걸쳐 공유된다.

```yml
variables:
  DOCKER_REGISTRY: registry.gitlab.com
  DOCKER_IMAGE_PREFIX: $DOCKER_REGISTRY/your-username/your-project
```

미리 정의된 변수를 다음에서 "persisted"라고 할 수도 있다.

> [Where variables can be used](https://docs.gitlab.com/ee/ci/variables/where_variables_can_be_used.html?source=post_page-----c08d887e13df--------------------------------#gitlab-ciyml-file)

*after_script*에서 **언급된(mentioned)** 변수만 사용할 수 있다.

이후 자격 증명이 설정된 상태에서 파이프라인의 각 작업 전에 실행해야 하는 명령을 정의하기 위해 일반적으로 `before_script` 섹션이 사용된다.

## `before_script`
`before_script`에 일반적인 설정 작업을 넣음으로써 파이프라인을 좀 더 체계적이고 이해하기 쉽게 만들 수 있다. 이는 본 작업을 시작하기 전에 해야 할 일들에 대한 체크리스트를 가지고 있는 것과 같다.

예를 들어 여러 작업에서 도커 이미지를 구축하고 테스트하는 경우 `before_script`를 사용하여 모든 작업이 동일한 인증을 사용할 수 있도록 도커 레지스트리에 로그인할 수 있다.

```yml
stages:
  - build
  - test

variables:
  DOCKER_REGISTRY: registry.gitlab.com
  DOCKER_IMAGE_PREFIX: $DOCKER_REGISTRY/your-username/your-project

before_script:
  - echo "Logging in to the Docker registry..."
  - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $DOCKER_REGISTRY

# log in to the Docker registry using the GitLab CI/CD job token. 
# This ensures that all jobs in the pipeline have the necessary authentication.

build-job:
  stage: build
  script:
    - echo "Building Docker image..."
    - docker build -t $DOCKER_IMAGE_PREFIX/service-name:$CI_COMMIT_REF_NAME .
    - docker push $DOCKER_IMAGE_PREFIX/service-name:$CI_COMMIT_REF_NAME

test-job:
  stage: test
  script:
    - echo "Running tests..."
    - # Commands to run tests
```

빌드 단계에서만 로그인하려면 어떻게 해야 할까? 다음과 같다.

```yml
before_script:
  - echo "Logging in to the Docker registry..."
  - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $DOCKER_REGISTRY
only:
  - build-job
```

또는 수행할 단계에 작성된 것이다.

```yml
build_job:
before_script:
  - echo "Logging in to the Docker registry..."
  - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $DOCKER_REGISTRY
script:
    - echo "Building Docker image..."
    - docker build -t $DOCKER_IMAGE_PREFIX/service-name:$CI_COMMIT_REF_NAME .
    - docker push $DOCKER_IMAGE_PREFIX/service-name:$CI_COMMIT_REF_NAME
```

그렇다면 다음 단계의 주요 관심사는 무엇일까?

```yml
- build
- deploy
- test
```

## `build`
'**build**'가 의미하는 바는 소스코드를 컴퓨터에서 실행하거나 서버에 배포할 수 있는 실행 가능한 형태로 변환하는 프로세스를 실행하는 것이다. 빌드 단계에서는 **소스코드를 컴파일하고**, **필요한 작업을 실행하며** **인공물(artifact)을 제작하는** 데 일차적인 초점을 둔다. 이 단계는 **배포를 위한 애플리케이션이나 소프트웨어를 준비하는** 단계이다.

```
┌───────────────────────────────────────┐
│         Source Code Repository        │
└───────────────────────────────────────┘
                    │
                    ▼
┌───────────────────────────────────────┐
│             CI/CD Pipeline            │
│     (Defined in .gitlab-ci.yml, etc.) │
└───────────────────────────────────────┘
                    │
                    ▼
┌───────────────────────────────────────┐
│              Build Stage              │
│  - Compile source code                │ #Compilation the key part 
│  - Package artifacts                  │  of build
└───────────────────────────────────────┘
                    │
                    ▼
┌───────────────────────────────────────┐
│          Test and Analysis            │
│  - Code quality checks (linters, etc.)│
│  - Static code analysis               │
└───────────────────────────────────────┘
                    │
                    ▼
┌───────────────────────────────────────┐
│          Publish Artifacts            │
│  - Store build artifacts (binaries)   │
│  - Create versioned releases          │
└───────────────────────────────────────┘
```

상위 레벨 소스 코드로부터 생성된 [아티팩트(artifact)](https://docs.gitlab.com/ee/ci/yaml/?query=artifacts)는 정적 리소스, 실행 파일, 구성 및 라이브러리를 포괄하는 번역 코드 또는 머신 코드(0s 및 1s)를 포함한다. 성공적인 CI/CD 파이프라인은 **효과적인 스토리지 관리 및 아카이빙 전략**을 필요로 하며, 아티팩트에서 **불필요하거나 큰 임시 파일을 제외하는** 것이 좋다.

당신의 명시적인 지시 없이 GitLab은 작업이나 단계 사이의 파일을 아티팩트로 자동 상정하거나 전달하지 **않는다**. 그것은 기본적으로 할당한 아티팩트만을 자동으로 처리할 것이다.

```yml
stages:
  - build
  - test

job1:
  stage: build
  script:
    - # Build commands
  artifacts:
    paths:
      - compiled_files/
      - documentation/
  # Creates artifacts from both places.
   expire_in: 1 week
# You can only download artifacts from the jobs listed in needs config.
job2:
  stage: build
  script:
    - # Build commands
  needs:
    - project: namespace/group/project-name
      job: job1  
      ref: main
      artifacts: true
  # Use artifacts: true (default) or false to control when artifacts are downloaded in jobs that use needs.

test:
  stage: test
  needs:
    - job: job1
      artifacts: true
```

- [`artifact:name`](https://docs.gitlab.com/ee/ci/yaml/?query=artifacts#artifactsname)과 함께 사용하지 않는 경우 artifact 파일의 이름은 `artifact`이며, 이 이름은 다운로드할 때 `artifact.zip`이 된다.
- `cache:paths` 키워드를 사용하고 `cache:key` 키워드를 추가하여 각 캐시에 고유한 식별 키를 부여한다. 캐시할 파일 또는 디렉터리를 선택할 수 있고 `cache:when`을 사용하여 캐시 저장 시기를 정의할 수도 있다.

## `deploy`
배포 단계에서는 컴파일된 코드, 라이브러리 및 기타 필요한 파일을 어플리케이션이 실행될 서버 또는 플랫폼으로 전송하는 것을 포함한다. 배포는 종종 구성 설정, 종속성 관리 및 어플리케이션이 프로덕션 환경에서 실행될 준비가 되었는지 확인하는 것과 같은 작업을 포함한다.

그것은 또한 일반적으로 다음과 같이 간주된다.

- `deploy-to-stage`라는 작업에서 `staging` 단계 
- `deploy-to-prod`라는 작업에서 `production` 단계

```yml
deploy_staging:
  stage: deploy
  script:
    - echo "Deploy to staging server"
  environment: #create an environment named staging, with URL 
    name: staging
    url: https://staging.example.com
```

여기서 언급해야 할 사항 중 하나가 인증(authentication)이다. AWS를 예로 들어보자. CI/CD 파이프라인과 서비스를 인증하고 GitLab CI/CD 파이프라인과 배포 서버 간의 통신을 보호하기 위해서는 AWS 액세스 키, 개인 SSH 키가 필요할 것이다.

![](./1_2bFnq-L5rSVBKiYGTme0Og.webp)

SCP(Secure Copy Protocol) 외에도 `rsync`는 변경된 부분만 전송하면서 파일을 점진적으로 업데이트할 수 있는 또 다른 파일 동기화 도구이다.

```yml
deploy_to_server:
  stage: deploy
  script:
    - rsync -avz -e "ssh -i /path/to/private/key" your_artifact.zip user@your-server:/path/on/server/
  only:
    - master
```

또는 AWS CLI(대상 서버가 AWS EC2인 경우).

```yml
deploy_to_aws:
  stage: deploy
  script:
    - aws s3 cp your_artifact.zip s3://your-s3-bucket/path/on/s3/
  only:
    - master
```

CI/CD Runner에서 사용하는 SSH 키가 대상 서버에 구성된 공개 키와 일치하는지 확인하도록 한다. 대상 서버에서 `~/.ssh/authorized_keys` 파일을 편집하여 CI/CD Runner의 공개 키를 추가할 수 있다.

> **Note**: 개인 키나 기타 민감한 정보가 노출되지 않도록 구성 시 보안 고려 사항을 확인하세요. GitLab은 CI/CD 파이프라인에 민감한 정보를 안전하게 저장하고 사용할 수 있는 "Secret Cariables"라는 CI/CD 기능을 제공한다.

```yml
deploy_to_aws:
  stage: deploy
  script:
    - scp -i $PRIVATE_SSH_KEY_PATH your_artifact.zip ec2-user@your-ec2-instance:/path/on/ec2/
    - ssh -i $PRIVATE_SSH_KEY_PATH ec2-user@your-ec2-instance "cd /path/on/ec2/ && unzip your_artifact.zip && ./start_app.sh"
  only:
    - master
  variables:
    PRIVATE_SSH_KEY_PATH: "$CI_PROJECT_DIR/private-key.pem"
```

## `test`

> " 테스트 뭐라고?! 난 제스트랑 정말 많은 걸 했어!"라고 불쌍한 선배에게 말했다.

GitLab CI/CD의 "[테스트 단계](https://docs.gitlab.com/ee/ci/testing/)"는 한 가지 유형의 테스트에만 국한된 것은 아니다. 코드베이스의 전체적인 평가를 제공하기 위해 다양한 테스트 도구를 포함한다. **페이지 접근성**, **브라우저에서 얼마나 잘 작동하는지**, **서버 성능에 미치는 영향**, **코드 적용 범위**, **코드 품질**, **라이센스 관리** 등을 확인할 수 있다. 각 도구는 코드 변경 사항이 견고한지 확인하는 데 특정한 역할을 한다.

테스트 단계에서는 CI/CD 파이프라인과 원활하게 통합되어 모든 코드 변경에 대해 일관성을 보장하는 것이 중요하다. 다음은 주요 고려 사항이다.

1. 종합 테스트 보고서 생성
1. 보고 매개 변수 정의
1. 적시 피드백을 위한 자동화
1. 테스트 결과에 쉽게 액세스

툴과 예시는 [여기](https://docs.gitlab.com/ee/ci/testing/)에서 찾아 볼 수 있다.
